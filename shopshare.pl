#!/usr/bin/perl

use Mojolicious::Lite;
use lib 'lib';
use utf8;
use Schema;
use Digest::SHA qw(sha256_base64);

# Configuracion de la app.
# Por ahora no lo uso, pero va a servir para cuando salte a prod

my $config = plugin Config => { file => 'shopshare.conf'};

# Usuarios del sistema.
# Tres niveles de acceso: guest, editor y admin.

my $userdb = {  admin => {
                  pass => 'cNs7ywbfrHO6U6jYhl2yjZqPMS7USW/LDr/s7JZc0pI',
                  level => 'admin',
                },

                guest => {
                  pass => 'm73b6N1j0mrfeF2GIvQRr1z9RMDBpT5dFlkBj93DVxo',
                  level => 'guest',
                },

                editor => {
                  pass => 's5FDfX+qkRVxuaR9OUCy1v7+KWOfqTPAzuZukaJYb20',
                  level => 'editor'
                },
              };

helper db => sub {
  return Schema->connect("dbi:SQLite:$config->{dbfile}","","", {
  sqlite_use_immediate_transaction => 1,
  sqlite_unicode => 1 });
};

helper auth => sub {
  my $c = shift;
  my $user = shift;
  my $pass = shift;

  return 1 if
    exists($userdb->{$user}) and
    $userdb->{$user}->{pass} eq sha256_base64($pass);

  return 0;
};

get '/' => 'index';

get '/logout' => sub {
  my $c = shift;
  $c->session(expires=> 1);
  $c->redirect_to('/');
};

get '/login' => 'login';

post '/login' => sub {
  my $c = shift;
  my $user = $c->param('username');
  my $pass = $c->param('password');
  if ($c->auth($user,$pass)) {
      $c->session(logged_in => 1);
      $c->session(username => $user);
      $c->session(userlevel => $userdb->{$user}->{level});
      $c->redirect_to('/');
  } else {
      $c->flash(mensaje => 'Usuario o Contraseña incorrecto');
      $c->redirect_to('/login');
  }
};

# Area restringida. Por debajo de este under hay que estar logeado.

under sub {
  my $c = shift;
  return 1 if $c->session('logged_in');

  $c->flash(mensaje => 'Area restringida. Por favor inicie sesion');
  $c->redirect_to('/login');
};

get '/usuarios' => 'usuarios';

get '/productos' => 'productos';

get '/compras' => 'compras';

get '/pedidos' => 'pedidos';

get '/pedidos/:nro/detalles' => 'detalle_pedidos';

# Por debajo de esto hay que ser admin o editor.

under sub {
  my $c = shift;

  return 1 if
      $c->session('userlevel') eq 'editor' or
      $c->session('userlevel') eq 'admin';

  $c->render('no-access');
  return 0;
};

get '/newpedido' => 'pedido_nuevo';

post '/newuser' => sub {
  my $c = shift;
  $c->db->resultset('Usuario')->create({
      nombre    => uc($c->param('nombre')),
      apellido  => uc($c->param('apellido')),
  });
  $c->render('usuarios');
};

post '/newprod' => sub {
  my $c = shift;
  $c->db->resultset('Producto')->create({
      nombre    => uc($c->param('nombre')),
      descripcion  => uc($c->param('descr')),
  });
  $c->redirect_to('/productos');
};

post '/newpedido' => sub {
  my $c = shift;
  my $id_user = $c->param('nombre');
  my $id_compra = $c->param('compra');
  my $max_nro_pedido = $c->db->resultset('Pedido')->get_column('nro_pedido')->max;
  $max_nro_pedido++;
  foreach my $ind (@{$c->req->params->names}) {
    if ($ind =~ /producto-(\d+)/) {
      my $id = $1;
      $c->db->resultset('Pedido')->create({
        usuario_id => $id_user,
        producto_id => $id,
        cantidad => $c->param($ind),
        nro_pedido => $max_nro_pedido,
        compra_id => $id_compra,
      });
    }
  }
  $c->redirect_to('pedidos');
};

post '/newcompra' => sub {
  my $c = shift;
  my $fecha = $c->param('fecha');
  my $descr = $c->param('descr');
  $c->db->resultset('Compra')->create({
      fecha => $fecha,
      comentario => $descr,
  });
  $c->redirect_to('/compras');
};

# Por debajo de este under hay que ser admin

under sub {
  my $c = shift;
  return 1 if $c->session('userlevel') eq 'admin';
  $c->render('no-access');
  return 0;
};

get '/pedidos/:id/borrar' => sub {
  my $c = shift;
  my $nro_pedido = $c->param('id');
  $c->db->resultset('Pedido')->search({ nro_pedido => $nro_pedido})->delete;
  $c->redirect_to('/pedidos');
};

get '/compras/:id/borrar' => sub {
  my $c = shift;
  my $id = $c->param('id');
  $c->db->resultset('Compra')->find($id)->delete;
  $c->redirect_to('/compras');
};

get '/usuarios/:id/borrar' => sub {
  my $c = shift;
  my $id = $c->param('id');
  $c->db->resultset('Usuario')->find($id)->delete;
  $c->redirect_to('/usuarios');
};

get '/productos/:id/borrar' => sub {
  my $c = shift;
  my $id = $c->param('id');
  $c->db->resultset('Producto')->find($id)->delete;
  $c->redirect_to('/productos');
};

app->defaults( layout => 'base');

app->mode('development');

app->secrets($config->{secrets});

app->start;

