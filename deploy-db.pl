#!/usr/bin/perl

use warnings;
use strict;

use lib "lib";
use Schema;

`mv database.db database.old`;

my $schema = Schema->connect('dbi:SQLite:database.db');
$schema->deploy({ add_drop_table => 1 });

