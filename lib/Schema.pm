use utf8;
package Schema;

use base 'DBIx::Class::Schema';

# Estes es el loader de la libreria de perl para base de datos.
# Es el standard. La definicion de las tablas esta en el directorio Schema.

__PACKAGE__->load_namespaces;

1;

