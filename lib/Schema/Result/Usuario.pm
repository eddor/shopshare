use utf8;
package Schema::Result::Usuario;

use base 'DBIx::Class::Core';

# Tabla: usuarios
# Uso: Lista de usuarios que participan y hacen productos.
#      ID usado como FK en tabla de pedidos.
#      En principio este ID no es el mismo que se usa para iniciar sesion
#      pero mas adelante probablemente si lo sea.
#      Deberiamos extender la tabla para que contenga los datos
#      del usuario, y el password.

__PACKAGE__->table('usuarios');

__PACKAGE__->add_columns(

  id => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  nombre => {
    data_type => 'text',
  },

  apellido => {
    data_type => 'text',
  },

);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    # Name of the accessor for this relation
    usuario =>
    # Foreign result class
    'Schema::Result::Pedido',
    # Foreign key in the table 'posts'
    'usuario_id'
);

1;

