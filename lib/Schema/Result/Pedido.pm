use utf8;
package Schema::Result::Pedido;

use base 'DBIx::Class::Core';

# Table: pedidos
# Uso: Esta tabla tiene todos los datos de las compras.
#      Utiliza todos los Foreign Keys, y relaciona los productos, usuarios
#      y fecha de compras.

__PACKAGE__->table('pedidos');

__PACKAGE__->add_columns(

  id => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  nro_pedido => {
    data_type => 'integer',
    is_nullable => 0,
  },

  usuario_id => {
    data_type => 'integer',
  },

  producto_id => {
    data_type => 'integer',
  },

  cantidad => {
    data_type => 'integer',
  },

  compra_id => {
    data_type => 'integer',
  },

);

# Tell DBIC that 'id' is the primary key
__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
  # Name of the accessor for this relation
  usuario =>
  # Foreign result class
  'Schema::Result::Usuario',
  # Foreign key in THIS table
  'usuario_id'
);

__PACKAGE__->belongs_to(
  # Name of the accessor for this relation
  producto =>
  # Foreign result class
  'Schema::Result::Producto',
  # Foreign key in THIS table
  'producto_id'
);

__PACKAGE__->belongs_to(
  # Name of the accessor for this relation
  compra =>
  # Foreign result class
  'Schema::Result::Compra',
  # Foreign key in THIS table
  'compra_id'
);

1;

