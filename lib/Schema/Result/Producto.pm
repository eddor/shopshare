use utf8;
package Schema::Result::Producto;

use base 'DBIx::Class::Core';

# Tabla: productos
# Uso: lista de productos con una descripcion.
#      el ID se usa como FK para relacionar los pedidos.


__PACKAGE__->table('productos');

__PACKAGE__->add_columns(

  id => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  nombre => {
    data_type => 'text',
  },

  descripcion => {
    data_type => 'text',
  },

);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    # Name of the accessor for this relation
    producto =>
    # Foreign result class
    'Schema::Result::Pedido',
    # Foreign key in the table 'posts'
    'producto_id'
);

1;
