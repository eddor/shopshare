use utf8;
package Schema::Result::Compra;

use base 'DBIx::Class::Core';

# Tabla: compras
# Uso: En esta tabla guardo la fecha y la descricion de la compra.
#      Seria el detalle de donde vamos a comprar y cuando.
#      En el caso que haya varias compras programadas, 
#      se puede elegir cuando trar tus productos.
# ForeignKey: el campo "id" se usa como primary key 
#             y como FK en la tabla de pedidos.
#             Muchos pedidos pueden apuntar a una sola compra.

__PACKAGE__->table('compras');

__PACKAGE__->add_columns(

  id => {
    data_type => 'integer',
    is_auto_increment => 1,
  },

  fecha => {
    data_type => 'text',
  },

  comentario => {
    data_type => 'text',
  },

);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    # Name of the accessor for this relation
    compra =>
    # Foreign result class
    'Schema::Result::Pedido',
    # Foreign key in the table 'posts'
    'compra_id'
);

1;

