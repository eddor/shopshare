#!/usr/bin/perl

use Mojolicious::Lite;
use lib '../lib';
use Schema;


helper db => sub {
  return Schema->connect('dbi:SQLite:../database.db',"","", {
  sqlite_use_immediate_transaction => 1,
  sqlite_unicode => 1});
};

get '/' => 'index';

app->start;

__DATA__

@@ index.html.ep

% my $o = app->db->resultset('Usuario');

% while (my $i = $o->next) {
%= $i->nombre;
%= $i->apellido;
% };

