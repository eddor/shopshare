#!/usr/bin/perl

use Mojolicious::Lite;

get '/' => 'index';

post '/dumper' => 'dumper';

app->start;

__DATA__

@@ index.html.ep

%= form_for '/dumper', method => 'post' => begin
  %= text_field 'nombre'
  %= text_field 'apellido'
  %= text_field 'edad'
  %= submit_button 'Enviar'
% end


@@ dumper.html.ep

%= dumper $self->req->params->names

