<h1>ShopShare</h1>

Una aplicacion para compartir las compras en supermercados mayoristas

<h2>Requerimientos</h2>

- Mojolicious
- DBIx::Class
- DBD::SQLite
- Bootstrap (incluido)
- jquery (incluido)
- jquery.quicksearch (incluido)

<h2>Instalacion</h2>

git clone https://gitlab.com/eddor/shopshare.git
cd shopshare/
morbo shopshare.pl

